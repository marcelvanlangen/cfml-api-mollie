component output=true accessors=true {

	// Vul hier de gegevens van jouw Mollie account.
	static {
		// opletten dat je hier de goede sleutel (live / test) gebruikt
		apiKey = "test_6njr5JaDf9kdyGfhRTsqJQUQNm32RT";
		profileID = "pfl_qhaSAkW34u";
		urlEndPointAPI = "https://api.mollie.com/v2/";
	}

	// This public function sends the logfile to Loggly and returns the resulting statuscode (hopefully '200 OK')
	/*public string function sendLogLine(required string logData) {
		if (!isJSON(arguments.logData)) {
			throw("Log data is not in JSON string notation");
		} else {
			httpService = new http(method="POST", charset="utf-8", url="#this.convertUrlEndPointSend(static.urlEndPointSend)#");
			httpService.addParam(name="content-type", type="header", value="application/x-www-form-urlencoded");
			httpService.addParam(name="Accept", type="header", value="application/x-www-form-urlencoded");
			httpService.addParam(encoded="false", type="body", value="#trim(arguments.logData)#");
			var result = httpService.send().getPrefix();
			return result.statuscode;
		}
	}*/

	/*public any function receiveLogLines(required string q, string from = "-24h", string until = "now", string order = "desc", string size = "50") {
		// Loggly is asked to make a search order, for which we'll get an RSID
		var logglyRsid = this.getLogglyResultset("#this.urlEndPointAPI(static.urlEndPointAPI)#search?q=#arguments.q#&from=#arguments.from#&until=#arguments.until#&order=#arguments.order#&size=#arguments.size#");
		var logglyRsidDes = DeserializeJSON(logglyRsid);
		// Loggly is asked to give the resulting resultset using the RSID as an identifier
		var resultSet = this.getLogglyResultset("#this.urlEndPointAPI(static.urlEndPointAPI)#events?rsid=#logglyRsidDes.rsid.id#");
		return resultSet;
	}*/

	// Deze (private) functie vraagt gegevens van Mollie op
	public any function getLogglyResultset(required string urlEndPoint) {
		httpService = new http(method="GET", charset="utf-8", username="#static.username#", password="#static.password#", url="#arguments.urlEndPoint#");
		httpService.addParam(name="content-type", type="header", value="application/x-www-form-urlencoded");
		httpService.addParam(name="Accept", type="header", value="application/x-www-form-urlencoded");
		httpService.addParam(name="Referer", type="header", value="#cgi.http_referer#");
		return httpService.send().getPrefix().filecontent;
	}

}